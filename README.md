# python使用PaddleOCR实现命名实体识别OCR

#### 1.简介：

PaddleOCR是飞桨（PaddlePaddle）推出的一个端到端的光学字符识别开源工具集，支持中文、英文、数字以及特殊符号等各种类型的文字检测、识别和词语整体识别。该工具集使用PaddlePaddle深度学习框架技术，提供了多种OCR模型和算法，包括基于CNN+CTC、DenseNet+CTC等模型，能够针对不同场景和应用提供最优的OCR解决方案。同时，PaddleOCR还集成了OCR精度评估工具，可以快速地评估OCR模型的准确率和鲁棒性。除此之外，PaddleOCR还提供了丰富的API接口和命令行工具，使得用户可以轻松地进行OCR应用的开发和部署。

PaddleOCR除了支持通用文字识别外，还具备大量针对特定领域或行业的OCR功能，例如身份证/银行卡实现、表格识别、汽车VIN码识别、发票识别、名片识别等。相比其他OCR工具，PaddleOCR在识别精度、效率和扩展性等方面都有着较好的表现和广泛的适用性，是目前业内较为流行和优秀的OCR工具之一。

#### 2.安装部署

pip install paddlepaddle -i https://pypi.tuna.tsinghua.edu.cn/simple/  
pip install shapely -i https://pypi.tuna.tsinghua.edu.cn/simple/  
pip install paddleocr -i https://pypi.tuna.tsinghua.edu.cn/simple/  

#### 3.使用说明

安装好上面的三个包之后，即可以直接运行ocr.py使用，2.png是待识别的图片

#### 4.tips:

1.识别时候三从上到下按行识别。

2.身份证/银行卡实现、表格识别、汽车VIN码识别、发票识别、名片识别等。相比其他OCR工具，PaddleOCR在识别精度、效率和扩展性等方面都有着较好的表现和广泛的适用性，是目前业内较为流行和优秀的OCR工具之一。

3.PaddleOCR 库可以处理倾斜、歪曲或旋转的图片。通过启用角度分类功能 (use_angle_cls=True)，PaddleOCR 可以检测并自动校正这些图像的角度。

#### 5.项目说明
直接调用了 PaddleOCR 提供的 API 进行文字识别，而不是基于一个已经训练好的模型进行微调。从没有输入模型名字也能够看出来。  

#### 6.项目csdn链接：
https://blog.csdn.net/weixin_44162814/article/details/135868977?spm=1001.2014.3001.5501

