from paddleocr import PaddleOCR
ocr = PaddleOCR(use_angle_cls=True, lang="ch")
#要识别图片的路径：
img_path = r"./2.png"
#识别结果：
result = ocr.ocr(img_path, cls=True)
#结果输出展示：
#for line in result[0]:
#    print(line)

boxes = []
txts = []
scores = []
for line in result[0]:
    txts.append(line[1][0])

print("txts:")
for i in range(len(txts)):
	#原格式文本输出
    print (txts[i])
